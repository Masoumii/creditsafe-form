$(document).ready(function() {
	// Show / Hide div based on radio button choice
	$("input[class='dbasics-radio']").on("click", function() {
		$("#csv").hide();
		$("#dbasics").show();
	});
	$("input[class='csv-radio']").on("click", function() {
		$("#dbasics").hide();
		$("#csv").show();
	});
});